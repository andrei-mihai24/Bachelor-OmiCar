var carImage = new Image();
carImage.src = 'http://i.imgur.com/eWquMHD.png';
function Vector(p1, p2) {
    this.x = p1.x - p2.x;
    this.y = p1.y - p2.y;

    this.normalize = function () {
        var norm = this;
        return {x: norm.x / norm.length(), y: norm.y / norm.length()};
    };
    this.length = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    };
}
function Point(x, y) {
    this.x = x;
    this.y = y;

    this.addVector = function (v) {
        this.x += v.x;
        this.y += v.y;
    };
    this.direction = null;
}

function lineDistance(point1, point2) {
    var xs = 0;
    var ys = 0;

    xs = point2.x - point1.x;
    xs = xs * xs;

    ys = point2.y - point1.y;
    ys = ys * ys;

    return Math.sqrt(xs + ys);
}

var canvas = document.getElementById('control');
var w = canvas.width = 850;
var h = canvas.height = 485;
var c = canvas.getContext('2d');

var turnLeft = false;
var turnRight = false;
var moveForward = false;
var moveBackwards = false;
var stop = false;

var pressedKey = false;

var parkmode = false;
var freeToPark = -1;   // 1 - free place to park on the left; 2 - free place to park on the right; 0 - go forward to search more
var destination = {x: 0, y: 0};

var destination103 = {x: 164, y: 20};
var destination102 = {x: 79, y: 20};
var destination101 = {x: 2, y: 20};

var destination106 = {x: 164, y: 300};
var destination105 = {x: 79, y: 300};
var destination104 = {x: 2, y: 300};

var turnLeft90degrees = false;
var turnRight90degrees = false;

var followLine = false;
var goIntoParkplace = false;

var adjustRightToStartSelfParking = false;
var adjustLeftToStartSelfParking = false;

document.onkeydown = function (e) {
    var mspeed = parseInt($('#mspeed').val());
    var gspeed = parseFloat($('#gspeed').val());
    
    if(mspeed > 200 ||  mspeed < 0)
        $('#mspeed').val("85");
    
    if(gspeed > 20 ||  gspeed < 0)
        $('#gspeed').val("1.5");

    if (e.keyCode > 26 && e.keyCode < 41)   //preventDefault - only for arrow keys and ESC and SPACE
        e.preventDefault();                 //else input field doesn't work
        

    if (parkmode) {
        if(e.keyCode == 27){
            parkmode = false;
            turnLeft = false;
            turnRight = false;
            moveForward = false;
            moveBackwards = false;
            stop = true;                                      
            $.ajax({
                 url: "http://austria.omilab.org/omirob/car1/rest/abort", data: "", type: "GET",
                 beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                 success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
            });
        }
        else return;
    }
    if (pressedKey)
        return;
    switch (e.keyCode) {

        case 37:
            if (!turnLeft)
                $.ajax({
                     url: "http://austria.omilab.org/omirob/car1/rest/driveLeft/" + $('#mspeed').val(), data: "", type: "GET",
                     beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                     success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
                });
            stop = false;
            turnLeft = true;
            pressedKey = true;
            break;
        case 39:
            if (!turnRight)
                $.ajax({
                     url: "http://austria.omilab.org/omirob/car1/rest/driveRight/" + $('#mspeed').val(), data: "", type: "GET",
                     beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                     success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
                });
            stop = false;
            turnRight = true;
            pressedKey = true;
            break;
        case 38:
            if (!moveForward)
                $.ajax({
                     url: "http://austria.omilab.org/omirob/car1/rest/driveForward/" + $('#mspeed').val(), data: "", type: "GET",
                     beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                     success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
                });
            stop = false;
            moveForward = true;
            pressedKey = true;
            break;
        case 40:
            if (!moveBackwards)
                $.ajax({
                     url: "http://austria.omilab.org/omirob/car1/rest/driveBackward/" + $('#mspeed').val(), data: "", type: "GET",
                     beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                     success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
                });
            stop = false;
            moveBackwards = true;
            pressedKey = true;
            break;
        case 32:
            if (!parkmode)
                $.ajax({
                     url: "http://austria.omilab.org/omirob/car1/rest/parkYourself/" + $('#mspeed').val(), data: "", type: "GET",
                     beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                     success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
                });
            parkmode = true;
            destination = destination106;
            if (car.rotation < -90 && car.rotation > -180 || car.rotation < 270 && car.rotation > 180)
                adjustRightToStartSelfParking = true;
            else if (car.rotation == -90 || car.rotation == 270)
                followLine = true;
            else adjustLeftToStartSelfParking = true;

            break;
    }
};
document.onkeyup = function (e) {
    if (parkmode)
        return;
    switch (e.keyCode) {
        case 37:
            $.ajax({
                 url: "http://austria.omilab.org/omirob/car1/rest/stop", data: "", type: "GET",
                 beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                 success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
            });
            turnLeft = false;
            stop = true;
            pressedKey = false;
            break;
        case 39:
            $.ajax({
                 url: "http://austria.omilab.org/omirob/car1/rest/stop", data: "", type: "GET",
                 beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                 success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
            });
            turnRight = false;
            stop = true;
            pressedKey = false;
            break;
        case 38:
            $.ajax({
                 url: "http://austria.omilab.org/omirob/car1/rest/stop", data: "", type: "GET",
                 beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                 success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
            });
            moveForward = false;
            stop = true;
            pressedKey = false;
            break;
        case 40:
            $.ajax({
                 url: "http://austria.omilab.org/omirob/car1/rest/stop", data: "", type: "GET",
                 beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                 success: function(data, textStatus, jqXHR) { console.log("Data: " + data); }
            });
            moveBackwards = false;
            stop = true;
            pressedKey = false;
            break;
    }
};

function Wheel(pivot, x, y) {
    this.xPos = x;
    this.yPos = y;
};
function Car() {
    this.wheelMaxAngle = 90;
    this.wheelAngle = 0;
    this.width = 51;
    this.height = 97;
    this.x = 80;
    this.y = 415;
    this.pivot = {x: this.x, y: this.y};
    this.pivotAxis = {x: 0, y: 52};

    this.speed = 0;
    this.rotation = 90;

    this.directionPivot = {x: this.pivotAxis.x, y: this.pivotAxis.y - 25};
    this.pivotAxisAbs = {
        x: 52 * Math.cos((this.rotation + 90) * Math.PI / 180) + this.pivot.x,
        y: 52 * Math.sin((this.rotation + 90) * Math.PI / 180) + this.pivot.y
    };
    this.tempDirPivot = {x: 0, y: 0};
    this.wheels = [];

    this.wheels.push(new Wheel(this.pivot, -27.5, 52));
    this.wheels.push(new Wheel(this.pivot, 32, 52));


    this.direction = (new Vector(this.directionPivot, this.pivotAxis)).normalize();

    this.updateDirection = function () {
        this.direction = (new Vector(this.directionPivot, this.pivotAxis)).normalize();
        this.direction.x *= this.speed;
        this.direction.y *= this.speed;

    };

    this.drawWheels = function () {
        for (var i = 0; i < this.wheels.length; i++) {
            var o = this.wheels[i];
            if (turnLeft || adjustLeftToStartSelfParking || turnLeft90degrees) {
                this.wheelAngle = -this.wheelMaxAngle;
            }
            else {
                this.wheelAngle = 0;
            }
            if (turnRight || adjustRightToStartSelfParking || turnRight90degrees) {
                this.wheelAngle = this.wheelMaxAngle;
            }
            c.save();
            /* Car Rotation */
            c.translate(this.pivot.x, this.pivot.y);
            c.rotate(this.rotation * Math.PI / 180);
            /* Wheel Rotation */
            c.translate(o.xPos, o.yPos);

            c.fillStyle = 'black';
            c.fillRect(-6, -20, 8, 31);
            c.restore();
        }

        this.directionPivot.x = 52 * Math.cos((this.wheelAngle + this.rotation - 90) * Math.PI / 180) + this.pivotAxis.x;
        this.directionPivot.y = 52 * Math.sin((this.wheelAngle + this.rotation - 90) * Math.PI / 180) + this.pivotAxis.y;

        this.tempDirPivot.x = 52 * Math.cos((this.wheelAngle - 90) * Math.PI / 180) + this.pivotAxis.x;
        this.tempDirPivot.y = 52 * Math.sin((this.wheelAngle - 90) * Math.PI / 180) + this.pivotAxis.y;
        this.updateDirection();
    };
    this.drawCar = function () {
        this.drawWheels();
        c.save();
        c.translate(this.pivot.x, this.pivot.y);
        c.rotate(this.rotation * Math.PI / 180);

        c.drawImage(carImage, -25, -10);
        c.beginPath();

        c.moveTo(this.pivotAxis.x - 25, this.pivotAxis.y);
        c.lineTo(this.pivotAxis.x + 25, this.pivotAxis.y);


        c.moveTo(this.pivotAxis.x, this.pivotAxis.y);
        c.lineTo(this.tempDirPivot.x, this.tempDirPivot.y);
        c.strokeStyle = "#fff";
        c.stroke();

        c.restore();
    };
    this.move = function () {

        if (moveForward || followLine || goIntoParkplace || turnRight || turnLeft)
            car.speed = $('#gspeed').val();
        if (moveBackwards)
            car.speed = - $('#gspeed').val();
        if (stop)
            car.speed = 0;

        if (parkmode) {
            car.speed = $('#gspeed').val();
            if (followLine && this.pivot.x <= destination.x) {

                var expectedTime = new Date().getTime() + 500;
                while (new Date().getTime() < expectedTime);
                if (freeToPark == -1 || freeToPark == 0)
                    $.ajax({
                         url: "http://austria.omilab.org/omirob/car1/rest/freeToPark", data: "", type: "GET",
                         beforeSend: function(xhr) {xhr.setRequestHeader("Authorization", "Bearer " + $('#token').val());},
                         success: function(data, textStatus, jqXHR) { freeToPark = data; }
                    });

                var expectedTime = new Date().getTime() + 1500;
                while (new Date().getTime() < expectedTime);

                if (freeToPark == "1") {
                    console.log("here 1");
                    followLine = false;
                    if (destination == destination106 || destination == destination103)
                        destination = destination106;
                    else if (destination == destination105 || destination == destination102)
                        destination = destination105;
                    else if (destination = destination104 || destination == destination101)
                        destination = destination104;
                    turnLeft90degrees = true;
                }
                else if (freeToPark == "2") {
                    console.log("here 2");
                    followLine = false;
                    if (destination == destination106 || destination == destination103)
                        destination = destination103;
                    else if (destination == destination105 || destination == destination102)
                        destination = destination102;
                    else if (destination = destination104 || destination == destination101)
                        destination = destination101;
                    turnRight90degrees = true;
                }
                else if (freeToPark == "0") {
                    console.log("here 3");
                    if (destination == destination106 || destination == destination103)
                        destination = destination105;
                    else
                        destination = destination104;
                    followLine = true;
                }
            }
            else if (goIntoParkplace && destination.y == 20 && this.pivot.y <= destination.y) {
                car.speed = 0;
                parkmode = false;
            }
            else if (goIntoParkplace && destination.y == 300 && this.pivot.y > destination.y) {
                car.speed = 0;
                parkmode = false;
            }
            else if (turnRight90degrees && (this.rotation > 0 && this.rotation < 45 || this.rotation > 360 )) {
                turnRight90degrees = false;
                goIntoParkplace = true;
                setTimeout(function () {
                }, 1000);
            }
            else if (turnLeft90degrees && (this.rotation < 180 && this.rotation > 90 || this.rotation < -270 && this.rotation > -360 )) {
                turnLeft90degrees = false;
                goIntoParkplace = true;
                setTimeout(function () {
                }, 1000);
            }
            else if (adjustRightToStartSelfParking && (this.rotation > -90 && this.rotation < 0 || this.rotation > 270 && car.rotation < 360)) {
                adjustRightToStartSelfParking = false;
                followLine = true;
            }
            else if (adjustLeftToStartSelfParking && (this.rotation < -90 && this.rotation > -180 || car.rotation > 180 && this.rotation < 270)) {
                adjustLeftToStartSelfParking = false;
                followLine = true;
            }
            else {
                if (this.pivot.x + this.direction.x >= 0 && this.pivot.x + this.direction.x <= 670
                    && this.pivot.y + this.direction.y >= 15 && this.pivot.y + this.direction.y <= 460) {
                    this.pivot.x += this.direction.x;
                    this.pivot.y += this.direction.y;
                }
                else
                    car.speed = 0;
            }
        }
        else {
            if (this.pivot.x + this.direction.x >= 0 && this.pivot.x + this.direction.x <= 670
                && this.pivot.y + this.direction.y >= 15 && this.pivot.y + this.direction.y <= 460) {
                this.pivot.x += this.direction.x;
                this.pivot.y += this.direction.y;
            }
            else
                car.speed = 0;
        }

        var v = new Vector(this.pivot, this.pivotAxisAbs);
        var rotation = Math.atan2(-v.y, v.x) * 180 / Math.PI;
        rotation += 180;
        rotation = 360 - rotation - 90;
        this.rotation = rotation;


        var pivotAxisAbs = {
            x: 52 * Math.cos((this.rotation + 90) * Math.PI / 180) + this.pivot.x,
            y: 52 * Math.sin((this.rotation + 90) * Math.PI / 180) + this.pivot.y
        };
        this.pivotAxisAbs = pivotAxisAbs;
    };
};

var car = new Car();

var loop = function () {
    var image = new Image();
    //image.src = 'http://i.imgur.com/9TfXVKw.png';
    image.src = 'http://i.imgur.com/8WmjkMP.png';
    image.onload = function () {
        c.drawImage(image, 0, 0);
        car.move();
        car.drawCar();
    };


};

(function () {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame =
            window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () {
                    callback(currTime + timeToCall);
                },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
}());

(function animloop() {
    window.animationId = window.requestAnimationFrame(animloop);
    loop();
})();