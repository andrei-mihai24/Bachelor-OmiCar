<!DOCTYPE html>
<html>
<head>
<title>Simple</title>
</head>
<body>
<script type="text/javascript" src="${staticpath}/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="${staticpath}/js/jquery.serializeObject.min.js"></script>

<div id="wrapper">
	<div id="legacywrapperouter" style="width:100%">  
		<p>Legacy Data</p>
		<div id="legacywrapper">
	
	  	</div> 
	</div>
	
	<div id="commentswrapperouter" style="width:100%">  
		<p>Comments</p>
		
		<form id="commentform">
		  <textarea id="comment" rows="4" cols="50"  name="value">Enter comment here...</textarea>
		  <br>
		  <input type="submit">
		</form>
		
		<p/>
		
		<div id="commentswrapper">
			<table id="comments">
	
			</table>
	  	</div> 
	</div>
</div>

<script>
	$(function(){
		$("#commentform").on("submit", function(e){
		    if( $("textarea#comment").val() != "" && $("textarea#comment").val() != "Enter comment here...") {
		        $.ajax({
		          type: "post",
		          url: window.location.origin + window.location.pathname + "/ajax/postcomment",
		          data: JSON.stringify($("#commentform").serializeObject()),
		          contentType: "application/json; charset=utf-8",
		          dataType: "json"
		         });
		    }
		    e.preventDefault();
		});
	});
	
	$("textarea#comment").focus(function() {
	    if( $(this).val() == "Enter comment here..." ) {
	        $(this).val("");
	    }
	});
	
	$("textarea#comment").blur(function() {
	    if( $(this).val() == "" ) {
	        $(this).val("Enter comment here...");
	    }
	});
	
	$(document).ready(function() {executeRefresh();})
	var comments = 0;
	
	function executeRefresh(refreshType) {
		$.ajax({
			type: "get",
			url: window.location.origin + window.location.pathname + "/ajax/getlegacy",
			success: function(data){
				executeRefreshLegacyDiv(data);
			}
		});
		
		$.ajax({
			type: "get",
			data: {id: comments},
			url: window.location.origin + window.location.pathname + "/ajax/getcomments",
			success: function(data){
				executeRefreshCommentsTable(data);
			}
		});

	}
	
	$(document).ajaxStop(function () {
		setTimeout(function() { executeRefresh();}, 500);
	});
	
	function executeRefreshLegacyDiv(data) {
		var tbl=$("<table/>").attr("id","legacy");
		$("#legacywrapper").html(tbl);
		for(var i=0;i<data.length;i++)
		{
			var tr="<tr>";
			var td1="<td>"+data[i]["id"]+"</td>";
			var td2="<td>"+data[i]["value"]+"</td></tr>";
	
			$("#legacy").append(tr+td1+td2); 
		}
	}
	
	function executeRefreshCommentsTable(data) {
		for(var i=0;i<data.length;i++)
		{
			var tr="<tr>";
			var td1="<td>"+data[i]["value"]+"</td></tr>";
	
			$("#comments").append(tr+td1); 
			comments = Math.max(comments, data[i]["id"]);
		}
	}
</script>

</body>
</html>