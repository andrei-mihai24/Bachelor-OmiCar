package org.omilab.omirob.webapp.simple.service;

import org.omilab.omirob.legacy.mbot.IMbotEvent;
import org.omilab.omirob.legacy.mbot.MbotClient;
import org.omilab.omirob.legacy.mbot.MbotConstants;
import org.springframework.stereotype.Service;
import purejavacomm.CommPortIdentifier;
import purejavacomm.NoSuchPortException;
import purejavacomm.PortInUseException;
import purejavacomm.UnsupportedCommOperationException;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.Enumeration;

@Singleton
@Service("simpleMgmtService")
public class SimpleMgmtServiceImpl implements SimpleMgmtService, IMbotEvent
{
    private MbotClient mc;
    private int freeToPark = -1;
    private boolean abort = false;

    public void init() throws NoSuchPortException, PortInUseException, IOException, UnsupportedCommOperationException
    {
        mc = new MbotClient(CommPortIdentifier.getPortIdentifier(MbotConstants.comPortName));
        mc.addListener(this);
        mc.reset();
    }

    public String randomNumberString()
    {
        return Double.toString(Math.random());
    }

    @Override
    public void onButton(boolean pressed)
    {
        System.out.println("button" + pressed);
    }

    private void trySleep(long ms)
    {
        try
        {
            Thread.sleep(ms);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public String printCommPorts()
    {
        Enumeration<CommPortIdentifier> identifiers = CommPortIdentifier.getPortIdentifiers();
        String s = "";
        StringBuilder sBuilder = new StringBuilder(s + "Detected com ports:\n");
        while (identifiers.hasMoreElements())
        {
            CommPortIdentifier id = identifiers.nextElement();
            sBuilder.append(String.format("Name: \"%s\", Type: %s, Owner: %s", id.getName(), id.getPortType(), id.isCurrentlyOwned() ? id.getCurrentOwner() : "-"));
            sBuilder.append('\n');
        }
        s = sBuilder.toString();
        return s;
    }

    public String readSensorData() throws IOException
    {
        String s = "";
        s = s + "Gyro Sensor: " + mc.readUltraSonic(MbotConstants.GyroPort) + " cm\n";
        s = s + "UltraSonicLeft Sensor: " + mc.readUltraSonic(MbotConstants.UltraSonicLeftPort) + " cm\n";
        s = s + "UltraSonicRight Sensor: " + mc.readUltraSonic(MbotConstants.UltraSonicRightPort) + " cm\n";
        s = s + "LineFollower Sensor: " + mc.readLineFollower(MbotConstants.LineFollowerPort) + "\n";
        return s;
    }

    public void terminate()
    {
        mc.close();
    }

    public void test() throws IOException
    {
        followLine(100);
        turnRight90Degrees(100);
        followLine(100);
        followLineBackwards(100);
        turnLeft90Degrees(100);
    }

    public void followLine(int speed) throws IOException
    {
        mc.followLine(speed);
    }

    public void followLineBackwards(int speed) throws IOException
    {
        mc.followLineBackwards(speed);
    }

    public void stop() throws IOException
    {
        mc.moveCar(0, 0);
    }

    public void turnRight90Degrees(int speed) throws IOException
    {
        mc.turnRight(87, speed);
        long expectedTime = System.currentTimeMillis() + 1000;
        while (System.currentTimeMillis() < expectedTime) ;
    }

    public void turnLeft90Degrees(int speed) throws IOException
    {
        mc.turnLeft(87, speed);
        long expectedTime = System.currentTimeMillis() + 1000;
        while (System.currentTimeMillis() < expectedTime) ;
    }

    public void moveBackwards(int speed) throws IOException
    {
        mc.moveCar(-speed, -speed);
    }

    public void moveForward(int speed) throws IOException
    {
        mc.moveCar(speed, speed);
    }

    public void moveLeft(int speed) throws IOException
    {
        mc.moveCar(-speed, +speed);
    }

    public void moveRight(int speed) throws IOException
    {
        mc.moveCar(speed, -speed);
    }

    // NELI's USE CASE from here
    public void moveDistance(int speed, double distance) throws IOException
    {
        mc.moveDistance(speed, distance);
    }

    public int isFreeToPark() throws IOException
    {
        return this.freeToPark;
    }

    public void setFreeToPark(int freeToPark) throws IOException
    {
        this.freeToPark = freeToPark;
    }

    public void parkYourself(int speed) throws IOException // NELI's USE CASE
    {

        followLine(speed);
        while (mc.readUltraSonic(MbotConstants.UltraSonicLeftPort) < 20D && mc.readUltraSonic(MbotConstants.UltraSonicRightPort) < 20D)
        {
            if (abort)
            {
                abort = false;
                return;
            }
            setFreeToPark(0);
            moveDistance(speed, 6.0);
            followLine(speed);
        }
        if (mc.readUltraSonic(MbotConstants.UltraSonicLeftPort) > 20D)
        {
            if (abort)
            {
                abort = false;
                return;
            }
            setFreeToPark(1);
            turnLeft90Degrees(speed);
            moveDistance(speed, 19.0);
        } else
        {
            if (abort)
            {
                abort = false;
                return;
            }
            setFreeToPark(2);
            turnRight90Degrees(speed);
            moveDistance(speed, 19.0);
        }
        setFreeToPark(-1);

    }

    public void abort() throws IOException 
    {
        abort = true;
        stop();
    }
    // NELI's USE CASE until here

    public void parkInSpot(int spot, int speed) throws IOException // ANDREI's USE CASE
    {
        mc.parkInSpot(spot, speed);
    }

    public int getParkedInSpot() // ANDREI's USE CASE
    {
        return mc.getParkedInSpot();
    }

    public void parkYourself2(int speed) throws IOException // ANDREI's USE CASE
    {
        mc.parkYourself2(speed);
    }


}
