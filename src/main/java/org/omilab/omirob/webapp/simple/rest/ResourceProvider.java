package org.omilab.omirob.webapp.simple.rest;

import java.io.InputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/")
public class ResourceProvider
{
    @GET
    @Path("/static/{filename:.*}")
    public Response getStaticFile(@PathParam("filename") String filename)
    {
        try
        {
            InputStream s = ResourceProvider.class.getResourceAsStream("/webapp/resources/static/" + filename);
            return Response.status(200).entity(s).build();
        } catch (Exception e)
        {
            return Response.serverError().entity(e.toString()).build();
        }
    }
}