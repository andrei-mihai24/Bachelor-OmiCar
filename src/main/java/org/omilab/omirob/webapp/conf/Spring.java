package org.omilab.omirob.webapp.conf;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("resources")
@ComponentScan("org.omilab.omirob.webapp")
public class Spring extends ResourceConfig {
	public Spring() {
		packages("org.omilab.omirob.webapp");
	}
}
