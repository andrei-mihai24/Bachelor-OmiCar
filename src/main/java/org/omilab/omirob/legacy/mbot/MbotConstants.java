package org.omilab.omirob.legacy.mbot;

/**
 * @author Andrei Nagy
 */
public class MbotConstants
{
    public static final double BOTH_ON_THE_LINE = 0D;
    public static final double BOTH_OFF_THE_LINE = 3D;
    public static final double RIGHT_ON_THE_LINE = 2D;
    public static final double LEFT_ON_THE_LINE = 1D;

    public static final String comPortName = "ttyUSB0";
    //public static final String comPortName = "tty.wchusbserial1410";
    public static final int UltraSonicForwardPort = 1;

    public static final int UltraSonicLeftPort = 1;
    public static final int UltraSonicRightPort = 3;
    public static final int LineFollowerPort = 2;
    public static final int GyroPort = 4;

    public static final double WHEEL_DIAMETER = 6.39D;
    public static final double PI = 3.1415D;

    // not a constant
    public static int parkedInSpot = -1;
}