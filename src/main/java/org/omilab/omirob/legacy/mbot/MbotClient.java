package org.omilab.omirob.legacy.mbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import purejavacomm.CommPortIdentifier;
import purejavacomm.PortInUseException;
import purejavacomm.SerialPort;
import purejavacomm.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Client library for original mBot firmware
 * Make sure matching mBlock firmware is running on your device (mbot_firmwae.ino, orion_firmware.ino, ...)
 *
 * @author Martin Kunz <martin.michael.kunz@gmail.com>
 * @author Andrei Nagy (updated)
 * @author Neli Petkove (updated)
 */
public class MbotClient implements Runnable, AutoCloseable
{
    private static final Logger log = LoggerFactory.getLogger(MbotClient.class);

    private static final byte TYPE_READ = 0x01;
    private static final byte TYPE_WRITE = 0x02;
    private static final byte TYPE_RESET = 0x04;
    private static final byte TYPE_START = 0x05;
    private static final byte MB_MCU_DATATYPE_ZERO = 0x00;
    private static final byte MB_MCU_DATATYPE_BYTE = 0x01;
    private static final byte MB_MCU_DATATYPE_FLOAT = 0x02;
    private static final byte MB_MCU_DATATYPE_SHORT = 0x03;
    private static final byte MB_MCU_DATATYPE_STRING = 0x04; //length(byte)+string
    private static final byte MB_MCU_DATATYPE_DOUBLE = 0x05;

    public final static int PORT_LIGHTSENSOR_INTERNAL = 6;
    public final static int RGB_INTERNAL_IDX = 2;
    public final static int LEDMATRIX_ACTION_STRING = 1;
    public final static int LEDMATRIX_ACTION_BITMAP = 2;
    public final static int LEDMATRIX_ACTION_CLOCK = 3;
    public final static byte REMOTE_LEFT = 0x07;
    public final static byte REMOTE_UP = 0x40;
    public final static byte REMOTE_RIGHT = 0x09;
    public final static byte REMOTE_DOWN = 0x19;
    public final static byte REMOTE_A = 0x45;
    public final static byte REMOTE_B = 0x46;
    public final static byte REMOTE_C = 0x47;
    public final static byte REMOTE_D = 0x44;
    public final static byte REMOTE_E = 0x43;
    public final static byte REMOTE_F = 0x0D;
    public final static byte REMOTE_OK = 0x15;
    public final static byte REMOTE_0 = 0x16;
    public final static byte REMOTE_1 = 0x0C;
    public final static byte REMOTE_2 = 0x18;
    public final static byte REMOTE_3 = 0x5E;
    public final static byte REMOTE_4 = 0x08;
    public final static byte REMOTE_5 = 0x1C;
    public final static byte REMOTE_6 = 0x5A;
    public final static byte REMOTE_7 = 0x42;
    public final static byte REMOTE_8 = 0x52;
    public final static byte REMOTE_9 = 0x4A;

    private final InputStream portIS;
    private final OutputStream portOS;
    private SerialPort serialPort;
    Set<LinkedBlockingQueue<Result>> subscribers = Collections.synchronizedSet(new HashSet(new LinkedBlockingQueue<>()));
    Set<IMbotEvent> externalSubscribers = Collections.synchronizedSet(new HashSet<>());
    private volatile boolean running = true;
    private Thread thread;

    // #define GET 1
    // #define RUN 2
    // #define RESET 4
    // #define START 5

    public MbotClient(CommPortIdentifier portid) throws PortInUseException, IOException, UnsupportedCommOperationException
    {
        //serialPort = (SerialPort) portid.open("asdf", 3500);
        serialPort = (SerialPort) portid.open("test", 3500);
        serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
        serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);
        serialPort.disableReceiveFraming();
        serialPort.disableReceiveThreshold();
        serialPort.disableReceiveTimeout();
        portIS = serialPort.getInputStream();
        portOS = serialPort.getOutputStream();
        thread = new Thread(this);
        thread.setName("Serial port read thread");
        thread.start();
    }

    @Override
    public void close()
    {
        running = false;
        thread.interrupt();
        serialPort.close();
    }

    public void addListener(IMbotEvent eventif)
    {
        externalSubscribers.add(eventif);
    }


    public void reset() throws IOException
    {
        waitForResult(TYPE_RESET, DeviceType.NONE, new byte[]{}, Void.class);
    }

    public float readUltraSonic(int port) throws IOException
    {
        Result r = waitForResult(TYPE_READ, DeviceType.ULTRASONIC_SENSOR, new byte[]{(byte) port}, Float.class);
        return (float) r.result;
    }

    public float readLineFollower(int port) throws IOException
    {
        Result r = waitForResult(TYPE_READ, DeviceType.LINEFOLLOWER, new byte[]{(byte) port}, Float.class);
        return (float) r.result;
    }

    /**
     * @param port light sensor port Internal: Nr. 6
     * @return intensity (0..1024)
     * @throws IOException
     */
    public float readLightSensor(int port) throws IOException
    {
        byte[] payload = new byte[1];
        payload[0] = (byte) (port & 0xFF);
        Result r = waitForResult(TYPE_READ, DeviceType.LIGHT_SENSOR, payload, Float.class);
        return (float) r.result;
    }

    public float readLightSensorOnboard() throws IOException
    {
        return readLightSensor(6);
    }

    public float readSoundSensor(int port) throws IOException
    {
        byte[] payload = new byte[1];
        payload[0] = (byte) (port & 0xFF);
        Result r = waitForResult(TYPE_READ, DeviceType.SOUND_SENSOR, payload, Float.class);
        return (float) r.result;
    }

    public float readPotentiometer(int port) throws IOException
    {
        byte[] payload = new byte[1];
        payload[0] = (byte) (port & 0xFF);
        Result r = waitForResult(TYPE_READ, DeviceType.POTENTIONMETER, payload, Float.class);
        return (float) r.result;
    }

    public void motorLeft(int speed) throws IOException
    {
        speed = -speed;
        byte[] payload = new byte[3];
        payload[0] = 0x09; //left motor port
        payload[1] = (byte) (speed & 0xFF);
        payload[2] = (byte) (speed >> 8 & 0xFF);
        Result r = waitForResult(TYPE_WRITE, DeviceType.MOTOR, payload, Void.class);
    }


    public void motorRight(int speed) throws IOException
    {
        byte[] payload = new byte[3];
        payload[0] = 0x0a; //right motor
        payload[1] = (byte) (speed & 0xFF);
        payload[2] = (byte) (speed >> 8 & 0xFF);
        Result r = waitForResult(TYPE_WRITE, DeviceType.MOTOR, payload, Void.class);
    }

    /**
     * @param idx (1: left led, 2: right led, 0: both)
     * @param r   red (0..255)
     * @param g   green (0..255)
     * @param b   blue(0..255)
     * @throws IOException
     */
    public void rgbLEDOnboard(int idx, int r, int g, int b) throws IOException
    {
        rgbLED(0x07, idx, r, g, b);
    }

    public void rgbLED(int port, int idx, int r, int g, int b) throws IOException
    {
        byte[] payload = new byte[6];
        payload[0] = (byte) (port & 0xFF); //port
        payload[1] = (byte) (MbotClient.RGB_INTERNAL_IDX & 0xFF); //slot
        payload[2] = (byte) (idx & 0xFF); //idx
        payload[3] = (byte) (r & 0xFF);
        payload[4] = (byte) (g & 0xFF);
        payload[5] = (byte) (b & 0xFF);
        Result rr = waitForResult(TYPE_WRITE, DeviceType.RGBLED, payload, Void.class);
    }

    public void tone(int hz, int tone_time) throws IOException
    {
        byte[] payload = new byte[3];
        payload[0] = (byte) (hz & 0xFF);
        payload[1] = (byte) (0);
        payload[2] = (byte) (tone_time & 0xFF);
        Result r = waitForResult(TYPE_WRITE, DeviceType.TONE, payload, Void.class);
    }

    public void ledMatrixString(int port, int x, int y, String s) throws IOException
    {
        byte[] bstring = s.getBytes();
        byte[] payload = new byte[5 + bstring.length];
        payload[0] = (byte) (port & 0xFF);
        payload[1] = (byte) (0x01);
        payload[2] = (byte) (x);
        payload[3] = (byte) (y);
        payload[4] = (byte) (bstring.length & 0xFF);
        for (int i = 0; i < bstring.length; i++)
            payload[5 + i] = bstring[i];
        Result r = waitForResult(TYPE_WRITE, DeviceType.LEDMATRIX, payload, Void.class);
    }

    public void ledMatrixBitmap(int port, int x, int y, byte[] bitmap) throws IOException
    {
        byte[] payload = new byte[5 + bitmap.length];
        payload[0] = (byte) (port & 0xFF);
        payload[1] = (byte) (0x02);
        payload[2] = (byte) (x);
        payload[3] = (byte) (y);
        for (int i = 0; i < bitmap.length; i++)
            payload[5 + i] = bitmap[i];

        Result r = waitForResult(TYPE_WRITE, DeviceType.LEDMATRIX, payload, Void.class);
    }

    public byte readIRCmd()
    {
        byte[] payload = new byte[1];
        payload[0] = 0x00;
        Result r = waitForResult(TYPE_READ, DeviceType.IRREMOTECODE, payload, Byte.class);
        return (byte) r.result;
    }

    public void irSend(String s)
    {
        byte[] bstring = s.getBytes();
        byte[] payload = new byte[1 + bstring.length];
        payload[0] = (byte) (bstring.length + 3 & 0xFF);
        for (int i = 0; i < bstring.length; i++)
            payload[1 + i] = bstring[i];
        Result r = waitForResult(TYPE_WRITE, DeviceType.IR, payload, Void.class);
    }


    public float readPirMotion(int port)
    {
        byte[] payload = new byte[1];
        payload[0] = (byte) (port & 0xFF);
        Result r = waitForResult(TYPE_READ, DeviceType.PIRMOTION, payload, Float.class);
        return (float) r.result;
    }

    public float readJoystick(int port, int slot)
    {
        byte[] payload = new byte[2];
        payload[0] = (byte) (port & 0xFF);
        payload[1] = (byte) (slot & 0xFF);
        Result r = waitForResult(TYPE_READ, DeviceType.JOYSTICK, payload, Float.class);
        return (float) r.result;
    }


    public byte readHumiture(int port, int idx)
    {
        byte[] payload = new byte[2];
        payload[0] = (byte) (port & 0xFF);
        payload[1] = (byte) (idx & 0xFF);
        Result r = waitForResult(TYPE_READ, DeviceType.HUMITURE, payload, Byte.class);
        return (byte) r.result;
    }

    public float readGyro(int port, int axis)
    {
        byte[] payload = new byte[2];
        payload[0] = (byte) (port & 0xFF);
        payload[1] = (byte) (axis & 0xFF);
        Result r = waitForResult(TYPE_READ, DeviceType.GYRO, payload, Float.class);
        return (float) r.result;
    }

    public void stepper(Port port, int maxspeed, int distance) throws IOException
    {
        byte[] payload = new byte[7];
        payload[0] = port.getId();
        payload[1] = (byte) (maxspeed & 0xFF);
        payload[2] = (byte) (maxspeed >> 8 & 0xFF);
        payload[3] = (byte) (distance & 0xFF);
        payload[4] = (byte) (distance >> 8 & 0xFF);
        payload[5] = (byte) (distance >> 16 & 0xFF);
        payload[6] = (byte) (distance >> 24 & 0xFF);
        Result r = waitForResult(TYPE_WRITE, DeviceType.STEPPER, payload, Void.class);
    }

    public void servo(Port port, Port slot, int degree) throws IOException
    {
        byte[] payload = new byte[3];
        payload[0] = port.getId();
        payload[1] = slot.getId();
        payload[2] = (byte) (degree);
        Result r = waitForResult(TYPE_WRITE, DeviceType.SERVO, payload, Void.class);
    }

    private void send(byte type, DeviceType deviceType, byte[] payload) throws IOException
    {
        byte[] data = new byte[6 + payload.length];
        data[0] = (byte) (0xFF);
        data[1] = (byte) (0x55);
        data[2] = (byte) (payload.length + 3);
        data[3] = (byte) (0); //index
        data[4] = type; //action
        data[5] = deviceType.getId(); //device
        System.arraycopy(payload, 0, data, 6, payload.length);
        portOS.write(data);
        portOS.flush();
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 3];
        for (int j = 0; j < bytes.length; j++)
        {
            int v = bytes[j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
            hexChars[j * 3 + 2] = ',';
        }
        return new String(hexChars);
    }

    private Result waitForResult(byte typeRW, DeviceType deviceType, byte[] payload, Class type)
    {
        LinkedBlockingQueue<Result> q = new LinkedBlockingQueue<>();
        subscribers.add(q);
        try
        {
            while (true)
            {
                send(typeRW, deviceType, payload);
                while (true)
                {
                    Result r = q.poll(200, TimeUnit.MILLISECONDS);
                    if (r == null)
                        break; //send again

                    if (r.result == null)
                    {
                        if (type == Void.class)
                            return r;
                    } else
                    {
                        if (type.isAssignableFrom(r.result.getClass()))
                        {
                            return r;
                        }
                    }
                }
            }
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        } finally
        {
            subscribers.remove(q);
        }
    }

    private Result readResponse() throws IOException
    {
        byte[] sync = new byte[2];
        while (true)
        {
            sync[0] = sync[1];
            sync[1] = (byte) portIS.read();
            if (Arrays.equals(sync, new byte[]{(byte) 0xFF, (byte) 0x55}))
                break;
        }

        byte[] first = new byte[2];
        first[0] = (byte) portIS.read();
        first[1] = (byte) portIS.read();
        if (Arrays.equals(first, new byte[]{(byte) 0x0d, (byte) 0x0a}))
        { //OK reply (Motors, ..?)
            Result r = new Result<Void>();
            return r;
        } else if (Arrays.equals(first, new byte[]{(byte) 0x80, (byte) 0x01}))
        { //device button press
            Result r = new Result<Boolean>();
            r.deviceType = DeviceType.BUTTON_INNER;
            r.result = portIS.read() == 1;
            readSuffix();
            return r;
        } else
        {
            byte index = first[0];
            byte type = first[1];
            switch (type)
            {
                case MB_MCU_DATATYPE_FLOAT:
                {//float
                    byte[] data = new byte[4];
                    portIS.read(data);
                    Result r = new Result<Float>();
                    r.result = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getFloat();
                    r.deviceType = DeviceType.getById(type);
                    readSuffix();
                    return r;
                }
                case MB_MCU_DATATYPE_BYTE:
                {//byte
                    byte res = (byte) portIS.read();
                    Result r = new Result<Byte>();
                    r.result = res;
                    r.deviceType = DeviceType.getById(type);
                    readSuffix();
                    return r;
                }
                default:
                    log.warn("Unknows Type: " + type);
                    Result r = new Result<Void>();
                    return r;
            }
        }
    }

    private void readSuffix() throws IOException
    {
        byte[] suffix = new byte[2]; //od 0a
        portIS.read(suffix);
    }

    @Override
    public void run()
    {
        try
        {
            while (running)
            {
                Result r = readResponse();
                for (LinkedBlockingQueue<Result> q : subscribers)
                {
                    q.put(r);
                }

                for (IMbotEvent s : externalSubscribers)
                {
                    if (r.deviceType == DeviceType.BUTTON_INNER)
                    {
                        s.onButton((Boolean) r.result);
                    }
                }
            }
        } catch (InterruptedException | IOException e)
        {
            running = false;
            log.info("Serialport Thread terminating");
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public void moveCar(int speedLeftWheel, int speedRightWheel) throws IOException
    {
        motorLeft(speedLeftWheel);
        motorRight(speedRightWheel);
    }

    public boolean turnRight(int degrees, int speed) throws IOException {
        if (degrees <= 0) return false;

        float beginZ = readGyro(MbotConstants.GyroPort, 3);
        if (beginZ > -90 && beginZ <= 180) {
            float whileCondition = beginZ - degrees + 1;
            moveCar(speed, -speed);
            while (readGyro(MbotConstants.GyroPort, 3) > whileCondition)
                if (readLineFollower(MbotConstants.LineFollowerPort) == MbotConstants.BOTH_ON_THE_LINE)
                    break;
            moveCar(0, 0);
        } else if (beginZ - degrees > -180) {
            float whileCondition = beginZ - degrees;
            moveCar(speed, -speed);
            while (readGyro(MbotConstants.GyroPort, 3) > whileCondition)
                if (readLineFollower(MbotConstants.LineFollowerPort) == MbotConstants.BOTH_ON_THE_LINE)
                    break;
            moveCar(0, 0);
        } else {
            float whileCondition = 360 - degrees + beginZ;
            moveCar(speed, -speed);
            while (readGyro(MbotConstants.GyroPort, 3) < 0) ;
            while (readGyro(MbotConstants.GyroPort, 3) > whileCondition)
                if (readLineFollower(MbotConstants.LineFollowerPort) == MbotConstants.BOTH_ON_THE_LINE)
                    break;
            moveCar(0, 0);
        }
        return true;
    }

    public boolean turnLeft(int degrees, int speed) throws IOException {
        if (degrees <= 0) return false;

        float beginPos = readGyro(MbotConstants.GyroPort, 3);
        float endpos = beginPos + degrees;
        float pos = beginPos;

        if (endpos > 179)
            endpos -= 358;

        moveCar(-speed, +speed);

        while (Math.abs(readGyro(MbotConstants.GyroPort, 3) - endpos) > 4) ;

        moveCar(0, 0);

        return true;
    }

    public void followLine(int speed) throws IOException
    {
        while (true)
        {
            double lineFollowerData = readLineFollower(MbotConstants.LineFollowerPort);

            if (lineFollowerData == MbotConstants.BOTH_ON_THE_LINE) // Both sensors are on the line
            {
                System.out.println("Both on the line: go forward");
                moveCar(speed, speed);
            } else if (lineFollowerData == MbotConstants.BOTH_OFF_THE_LINE) // Both sensors are off the line
            {
                System.out.println("Both off the line: stop");
                moveCar(0, 0);
                break;
            } else if (lineFollowerData == MbotConstants.RIGHT_ON_THE_LINE) // The right sensor is on the line
            {
                System.out.println("Right on the line: go left");
                moveCar((int) (1.5 * speed), (int) (0.5 * speed));
            } else if (lineFollowerData == MbotConstants.LEFT_ON_THE_LINE) // The left sensor is on the line
            {
                System.out.println("Left on the line: go right");
                moveCar((int) (0.5 * speed), (int) (1.5 * speed));
            }
        }
        long expectedTime = System.currentTimeMillis() + 1000;
        while (System.currentTimeMillis() < expectedTime) ;
    }

    public void followLineBackwards(int speed) throws IOException
    {
        long expectedTime = System.currentTimeMillis() + 500;
        moveCar(-speed, -speed);
        while (System.currentTimeMillis() < expectedTime) ;
        followLine(-speed);
    }

    public void moveDistance(int speed, double distance) throws IOException
    {
        double rotation = MbotConstants.PI * MbotConstants.WHEEL_DIAMETER;
        double rotations_count = distance / rotation;
        double mseconds_needed = rotations_count / (speed / 60) * 1.3 * 1000;
        long expectedTime = System.currentTimeMillis() + (long) mseconds_needed;
        moveCar(speed, speed);
        while (System.currentTimeMillis() < expectedTime) ;
        moveCar(0, 0);
    }

    public void parkYourself2(int speed) throws IOException // ANDREI's USE CASE
    {
        MbotConstants.parkedInSpot = -1;
        followLine(speed);
        turnLeft(87, speed);
        followLine(speed);
        turnLeft(87, speed);
        followLine(speed);

        // Parking spots 106 (left) and 103 (right)
        if (readUltraSonic(MbotConstants.UltraSonicLeftPort) > 20D)
        {
            turnLeft(87, speed);
            moveDistance(speed, 20D);
            MbotConstants.parkedInSpot = 6; // 106
            return;
        } else if (readUltraSonic(MbotConstants.UltraSonicRightPort) > 20D)
        {
            turnRight(87, speed);
            moveDistance(speed, 20D);
            MbotConstants.parkedInSpot = 3; // 103
            return;
        } else
        {
            moveDistance(speed, 8.0);
            followLine(speed);
        }

        // Parking spots 105 (left) and 102 (right)
        if (readUltraSonic(MbotConstants.UltraSonicLeftPort) > 20D)
        {
            turnLeft(87, speed);
            moveDistance(speed, 20D);
            MbotConstants.parkedInSpot = 5; // 105
            return;
        } else if (readUltraSonic(MbotConstants.UltraSonicRightPort) > 20D)
        {
            turnRight(87, speed);
            moveDistance(speed, 20D);
            MbotConstants.parkedInSpot = 2; // 102
            return;
        } else
        {
            moveDistance(speed, 8.0);
            followLine(speed);
        }

        // Parking spots 104 (left) and 101 (right)
        if (readUltraSonic(MbotConstants.UltraSonicLeftPort) > 20D)
        {
            turnLeft(87, speed);
            moveDistance(speed, 20D);
            MbotConstants.parkedInSpot = 4; // 104
            return;
        } else if (readUltraSonic(MbotConstants.UltraSonicRightPort) > 20D)
        {
            turnRight(87, speed);
            moveDistance(speed, 20D);
            MbotConstants.parkedInSpot = 1; // 101
            return;
        } else turnRight(174, speed); // should be full 180 degree turn
    }

    public int getParkedInSpot()  // ANDREI's USE CASE
    {
        return MbotConstants.parkedInSpot;
    }

    public void parkInSpot(int spot, int speed) throws IOException  // ANDREI's USE CASE
    {
        MbotConstants.parkedInSpot = -1;
        followLine(speed);
        turnLeft(87, speed);
        followLine(speed);
        turnLeft(87, speed);
        followLine(speed);
        switch (spot)
        {
            case 6:
                turnAndPark(speed, 6); // left
                break;
            case 3:
                turnAndPark(speed, 3); // right
                break;
            case 5:
                moveDistance(speed, 8.0);
                followLine(speed);
                turnAndPark(speed, 5); // left
                break;
            case 2:
                moveDistance(speed, 8.0);
                followLine(speed);
                turnAndPark(speed, 2); // right
                break;
            case 4:
                moveDistance(speed, 8.0);
                followLine(speed);
                moveDistance(speed, 8.0);
                followLine(speed);
                turnAndPark(speed, 4); // left
                break;
            case 1:
                moveDistance(speed, 8.0);
                followLine(speed);
                moveDistance(speed, 8.0);
                followLine(speed);
                turnAndPark(speed, 1); // right
                break;
        }
    }

    private void turnAndPark(int speed, int spot) throws IOException // 1 is left, 2 is right
    {
        if (spot == 6 || spot == 5 || spot == 4)
        {
            if (readUltraSonic(MbotConstants.UltraSonicLeftPort) > 20D)
            {
                turnLeft(87, speed);
                moveDistance(speed, 20D);
                MbotConstants.parkedInSpot = spot;

            } else MbotConstants.parkedInSpot = -2;
        } else if (readUltraSonic(MbotConstants.UltraSonicRightPort) > 20D)
        {
            turnRight(87, speed);
            moveDistance(speed, 20D);
            MbotConstants.parkedInSpot = spot;
        } else MbotConstants.parkedInSpot = -2;
    }
}
